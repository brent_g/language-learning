import React, { Fragment } from "react";
import Joi from "joi-browser";
import ObjectId from "bson-objectid";

import Form from "../common/form";
import Title from "../common/title";
import CourseFormTable from "./courseFormTable";

// services
import { getCourse, saveCourse } from "../../services/courseService";
import CourseFormControls from "./courseFormControls";
import { toast } from "react-toastify";

class CourseForm extends Form {
  state = {
    data: { name: "", words: [] },
    errors: {}
  };

  async componentDidMount() {
    await this.populateCourseForm();
  }

  schema = {
    _id: Joi.string(),
    name: Joi.string()
      .required()
      .label("Name"),
    words: Joi.array()
      .items(Joi.object())
      .required()
      .min(1)
      .label("Words")
  };

  // we use a getter because the ObjectId() wont return a unique value otherwise
  // we use the _id for the tr's key value which is required by react to be unique
  // this also creates an _id specific to this word object to save into the db
  wordTemplate = {
    get value() {
      return {
        _id: new ObjectId().str,
        origin: "",
        target: "",
        alternate: [],
        type: [],
        tags: []
      };
    }
  };

  async populateCourseForm() {
    try {
      const courseId = this.props.match.params.id;
      // if there is no :id param it means we are making a new course then dont load any course data
      if (!courseId) return;

      const { data: course } = await getCourse(courseId);
      // use the maptoviewmodel to load the required fields into the state data object
      // and to get rid of unused mongo db fields (ex. __v)
      this.setState({ data: this.mapToViewModel(course) });
    } catch (ex) {
      // if the course with the given id doesnt exist, redirect to 404 page
      if (ex.response && ex.response.status === 404) {
        this.props.history.replace("/not-found");
      }
    }
  }

  mapToViewModel(course) {
    return {
      _id: course._id,
      name: course.name,
      words: course.words
    };
  }

  doSubmit = async () => {
    // validate each row
    // throw errors
    // save the data
    const result = this.courseSave();
    console.log(result);
    // return any errors
    // show completion message
    toast.success("Course Saved");
    //this.props.history.replace("/courses");
  };

  handleAddWord = e => {
    e.preventDefault();
    this.addWordRow();
  };

  handleCancel = e => {
    e.preventDefault();
    this.props.history.push("/courses");
  };

  handleDeleteWordRow = (e, word) => {
    e.preventDefault();
    this.deleteWordRow(word);
    toast.warn("Word Deleted");
  };

  courseSave = async () => {
    try {
      return await saveCourse(this.state.data);
    } catch (ex) {
      console.log("Exception hit while saving.", ex);
    }
  };

  addWordRow = () => {
    const data = { ...this.state.data };
    const template = { ...this.wordTemplate.value };

    data.words.push(template);

    this.setState({ data });
  };

  deleteWordRow = word => {
    const data = { ...this.state.data };
    data.words = data.words.filter(i => i !== word);
    this.setState({ data });
  };

  render() {
    const { data } = this.state;

    return (
      <Fragment>
        <div className="row">
          <div className="col-6">
            <Title
              text={this.props.match.params.id ? "Edit Course" : "New Course"}
            />
          </div>

          <div className="col-6">
            <div className="h-100 d-flex justify-content-end align-items-center">
              <CourseFormControls
                addWord={this.handleAddWord}
                cancel={this.handleCancel}
                save={this.doSubmit}
              />
            </div>
          </div>
        </div>

        <form onSubmit={this.handleSubmit}>
          <Fragment>
            <div className="form-group row">
              <div className="col-6">
                {this.renderInput("name", "Course Name")}
              </div>
            </div>
          </Fragment>

          <CourseFormTable
            data={data}
            deleteWordRow={this.handleDeleteWordRow}
          />
        </form>
      </Fragment>
    );
  }
}

export default CourseForm;
