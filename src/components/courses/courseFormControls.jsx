import React, { Fragment, Component } from "react";

class CourseFormControls extends Component {
  render() {
    return (
      <Fragment>
        <button className="btn btn-success m-2" onClick={this.props.addWord}>
          <i className="fa fa-plus" /> New Word
        </button>
        <button className="btn btn-warning m-2" onClick={this.props.cancel}>
          Cancel
        </button>
        <button className="btn btn-primary m-2" onClick={this.props.save}>
          Save
        </button>
      </Fragment>
    );
  }
}

export default CourseFormControls;
