import React from "react";
import Creatable from "react-select/lib/Creatable";

const CourseFormTableRow = ({
  word,
  count,
  wordExchange,
  valueChange,
  deleteWordRow
}) => {
  const altSpellingProps = {
    isSearchable: true,
    isMulti: true,
    filterOption: i => {
      return i;
    }
  };

  const selectProps = {
    isSearchable: true,
    isMulti: true,
    filterOption: i => {
      return i;
    }
  };

  return (
    <tr>
      <td>
        <input
          className="form-control"
          type="text"
          placeholder="From"
          value={word.origin}
          onChange={e => {
            e.preventDefault();
            valueChange(word, "origin", e.currentTarget.value);
          }}
        />
      </td>
      <td>
        <button
          className="btn btn-info"
          type="button"
          onClick={e => {
            e.preventDefault();
            wordExchange(word);
          }}>
          <i className="fa fa-exchange" />
        </button>
      </td>
      <td>
        <input
          className="form-control"
          type="text"
          placeholder="To"
          value={word.target}
          onChange={e => {
            e.preventDefault();
            valueChange(word, "target", e.currentTarget.value);
          }}
        />
      </td>
      <td>
        <Creatable
          style={{ "text-overflow": "ellipsis" }}
          value={word.alternate}
          {...altSpellingProps}
          onChange={value => {
            valueChange(word, "alternate", value);
          }}
        />
      </td>
      <td>
        <Creatable
          style={{ "text-overflow": "ellipsis" }}
          value={word.tags}
          {...selectProps}
          onChange={value => {
            valueChange(word, "tags", value);
          }}
        />
      </td>
      {count > 1 && (
        <td>
          <button
            className="btn btn-danger"
            type="button"
            onClick={e => deleteWordRow(e, word)}>
            <i className="fa fa-trash" />
          </button>
        </td>
      )}
    </tr>
  );
};

export default CourseFormTableRow;
