import React, { Fragment } from "react";
import { Link } from "react-router-dom";

const CourseContinueButton = ({ selectedCourse }) => {
  return (
    <Fragment>
      <div className="row justify-content-center">
        <div className="col-2">
          <Link to={`/courses/${selectedCourse._id}`}>
            <button className="btn btn-primary btn-lg" type="button">
              Continue
            </button>
          </Link>
        </div>
      </div>
    </Fragment>
  );
};

export default CourseContinueButton;
