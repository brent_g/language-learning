import React, { Component } from "react";
import ObjectId from "bson-objectid";

import CourseFormTableRow from "./courseFormTableRow";

class CourseFormTable extends Component {
  state = {};

  columns = [
    { path: "native", label: "Native" },
    { path: "", label: "" },
    { path: "practice", label: "Practice" },
    { path: "alternate", label: "Alternate" },
    { path: "tags", label: "Tags" },
    { path: "", label: "" }
  ];

  handleWordExchange = word => {
    const data = { ...this.props.data };
    const wordIndex = data.words.indexOf(word);

    // using temp variables is necessary to avoid a reference value error
    // where the origin will become the target but the target will stay the same value
    let tempOrigin = word.origin;
    let tempTarget = word.target;

    // replace the targets value with the origins value
    data.words[wordIndex].origin = tempTarget;
    data.words[wordIndex].target = tempOrigin;

    this.setState({ data });
  };

  handleFieldValueChange = (word, field, value) => {
    const data = { ...this.props.data };
    const wordIndex = data.words.indexOf(word);
    data.words[wordIndex][field] = value;
    this.setState({ data });
  };

  render() {
    const { data } = this.props;

    return (
      <table className="table">
        <thead>
          <tr>
            <th scope="col" style={{ width: "22%" }}>
              Native
            </th>
            <th scope="col" style={{ width: "6%" }} />
            <th scope="col" style={{ width: "22%" }}>
              Pratice
            </th>
            <th scope="col" style={{ width: "22%" }}>
              Alternate
            </th>

            <th scope="col" style={{ width: "22%" }}>
              Tags
            </th>
            <th scope="col" style={{ width: "6%" }} />
          </tr>
        </thead>
        <tbody className="table-striped">
          {data.words.map(wordObject => {
            return (
              <CourseFormTableRow
                key={wordObject._id || ObjectId()}
                count={data.words.length}
                word={wordObject}
                wordExchange={this.handleWordExchange}
                valueChange={this.handleFieldValueChange}
                deleteWordRow={this.props.deleteWordRow}
              />
            );
          })}
        </tbody>
      </table>
    );
  }
}

export default CourseFormTable;
