import React, { Component } from "react";
import Table from "../common/table";

class CourseSelectTable extends Component {
  // this columns object is passed to the TableHeader to configure proper theads w. sorting
  // we dont need to add this array to the state because it will not change during its life cycle
  columns = [
    { path: "origin", label: "Word" },
    { path: "target", label: "Translation" },
    {
      content: word => this.arrayItemsToString(word.alternate),
      key: item => item.value,
      label: "Tags"
    },
    {
      content: word => this.arrayItemsToString(word.tags),
      key: item => {
        console.log("item is", item);
        return item.value;
      },
      label: "Tags"
    }
  ];

  arrayItemsToString(arr) {
    return arr
      .reduce((acc, tag) => {
        acc.push(tag.label);
        return acc;
      }, [])
      .join(", ");
  }

  render() {
    const { words, sortColumn, onSort } = this.props;

    return (
      <Table
        columns={this.columns}
        sortColumn={sortColumn}
        data={words}
        onSort={onSort}
      />
    );
  }
}

export default CourseSelectTable;
