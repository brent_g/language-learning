import React, { Fragment } from "react";
import { Link } from "react-router-dom";

const CourseTableControls = ({ selectedCourse, deleteCourse }) => {
  return (
    <Fragment>
      <div className="m-2">
        <Link to={`/courses/edit/${selectedCourse._id}`}>
          <button type="button" className="btn btn-warning">
            <i className="fa fa-pencil" /> Edit
          </button>
        </Link>
      </div>
      <div className="m-2">
        <button type="button" className="btn btn-danger" onClick={deleteCourse}>
          <i className="fa fa-trash" /> Delete
        </button>
      </div>
    </Fragment>
  );
};

export default CourseTableControls;
