import React, { Fragment } from "react";
import { Link } from "react-router-dom";

const CourseNewButton = () => {
  return (
    <Fragment>
      <div className="text-center">
        <Link to={{ pathname: "/courses/new" }}>
          <button className="btn btn-success my-3 align-center">
            <i className="fa fa-plus" /> New Course
          </button>
        </Link>
      </div>
    </Fragment>
  );
};

export default CourseNewButton;
