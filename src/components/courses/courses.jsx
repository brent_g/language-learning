// modules
import React, { Component, Fragment } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import _ from "lodash";

// components
import TableItemCount from "../common/tableItemCount";
import Title from "../common/title";

import CourseSelectTable from "./courseSelectTable";
import CourseTableControls from "./courseTableControls";
import Pagination from "../common/pagination";
import ListGroup from "../common/listGroup";
import { paginate } from "../../utils/paginate";

// styles
import "react-tabs/style/react-tabs.css";

// services
import { getCourses, deleteCourse } from "../../services/courseService";
import CourseContinueButton from "./courseContinueButton";
import CourseNewButton from "./courseNewButton";
import { toast } from "react-toastify";

class Courses extends Component {
  state = {
    words: [],
    courses: [],
    selectedCourse: {},
    currentPage: 1,
    pageSize: 10,
    sortColumn: {
      path: "origin",
      order: "asc"
    },
    tabIndex: 0
  };

  componentDidMount() {
    this.getCoursesData();
  }

  async getCoursesData() {
    const { data: courses } = await getCourses();
    this.setState({
      courses,
      words: courses[0].words,
      selectedCourse: courses[0]
    });
  }

  //pagination
  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  // change the selected course depending on what the user selects
  handleCourseSelect = course => {
    this.setState({
      selectedCourse: course,
      words: course.words,
      currentPage: 1
    });
  };

  // sorting columns
  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  handleCourseDelete = async () => {
    // show prompt
    const response = window.confirm(
      "Are you sure you want to delete this course?"
    );

    if (response) {
      this.deleteCourse();
      toast.warn("Course Deleted.");
    }
  };

  deleteCourse = async () => {
    const { selectedCourse, courses } = this.state;
    const original = [...courses];
    let courseData = [...courses];

    try {
      courseData = courseData.filter(i => i !== selectedCourse);
      await deleteCourse(selectedCourse._id);
    } catch (ex) {
      console.log("Something to delete", ex);
      courseData = original;
    }
    this.setState({ courses: courseData });
  };

  setSelectedCourse = course => {
    let selected = {};

    if (course) {
      selected = course;
      return;
    }

    if (this.state.courses.length > 0) {
      this.setState({ selected });
    }
  };

  // get and filter/process the data to display in the table
  getPagedData = () => {
    const {
      words: allWords,
      pageSize,
      currentPage,
      selectedCourse,
      sortColumn
    } = this.state;

    // step 1: filter by course
    const filtered =
      selectedCourse && selectedCourse.id
        ? allWords.filter(word => word.course.id === selectedCourse.id)
        : allWords;

    // step 2: Sorting
    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);

    // step 3: paginate returns the appropriate words from specified page and pageSize
    const words = paginate(sorted, currentPage, pageSize);

    return {
      totalCount: filtered.length,
      data: words,
      filtered
    };
  };

  render() {
    const {
      courses,
      pageSize,
      currentPage,
      selectedCourse,
      sortColumn
    } = this.state;

    const { totalCount, data: words } = this.getPagedData();

    return (
      <Fragment>
        <div className="row">
          <div className="col-4">
            <Title text="Courses" />
          </div>
          <div className="col-8">
            <div className="h-100 d-flex justify-content-end align-items-center">
              <CourseTableControls
                selectedCourse={selectedCourse}
                deleteCourse={this.handleCourseDelete}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-4">
            <ListGroup
              items={courses}
              textProperty="name"
              valueProperty="_id"
              selectedItem={selectedCourse}
              onItemSelect={this.handleCourseSelect}
            />

            <CourseNewButton />
          </div>
          <div className="col-8">
            <Tabs
              selectedIndex={this.state.tabIndex}
              onSelect={tabIndex => this.setState({ tabIndex })}>
              <TabList>
                <Tab>Words</Tab>
              </TabList>
              <TabPanel>
                <TableItemCount totalCount={totalCount} />
                <CourseSelectTable
                  words={words}
                  sortColumn={sortColumn}
                  onWordCheck={this.handleWordCheck}
                  onSort={this.handleSort}
                />
                <Pagination
                  itemsCount={totalCount}
                  pageSize={pageSize}
                  currentPage={currentPage}
                  onPageChange={this.handlePageChange}
                />
                <CourseContinueButton selectedCourse={selectedCourse} />
              </TabPanel>
            </Tabs>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Courses;
