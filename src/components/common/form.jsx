import React, { Component } from "react";
import Joi from "joi-browser";
import Input from "./input";

class Form extends Component {
  state = {
    data: {},
    errors: {}
  };

  validate = () => {
    const options = { abortEarly: false }; // prevent Joi from returning on the first found error
    console.log("validate", this.state.data);
    const { error } = Joi.validate(this.state.data, this.schema, options);

    if (!error) return null;

    const errors = {};
    // loop over each error in the details array and add them to the errors object
    for (let { path, message } of error.details) {
      errors[path[0]] = message;
    }

    return errors;
  };

  // this validates an individual property and returns any errors
  validateProperty = ({ name, value }) => {
    // example of computed property
    const obj = { [name]: value };

    if (!this.schema) throw new Error("Schema is required for validation");

    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const errorMessage = this.validateProperty(input);

    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];

    const data = { ...this.state.data };
    data[input.name] = input.value;

    this.setState({ data, errors });
  };

  handleSubmit = e => {
    e.preventDefault();
    // validate the form, if we have any errors then dont submit to the server
    const errors = this.validate();
    this.setState({ errors: errors || {} });
    console.log("errors", errors);
    if (errors) return;

    this.doSubmit();
  };

  renderSubmitButton(label, props) {
    return (
      <button
        className="btn btn-primary form-control"
        // disabled={this.validate()}
        {...props}
        type="submit">
        {label}
      </button>
    );
  }

  renderButton(label, handleClick, props) {
    return (
      <button
        className="btn btn-primary form-control"
        onClick={handleClick}
        {...props}
        type="button">
        {label}
      </button>
    );
  }

  renderInput(name, label, type = "text") {
    const { data, errors } = this.state;
    return (
      <Input
        name={name}
        placeholder={label}
        error={errors[name]}
        type={type}
        label={label}
        value={data[name]}
        onChange={this.handleChange}
      />
    );
  }
}

export default Form;
