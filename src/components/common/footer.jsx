import React from "react";
import { Component } from "react";

class Footer extends Component {
  state = {};
  render() {
    return (
      <footer className="mastfoot mt-auto">
        <div className="inner" />
      </footer>
    );
  }
}

export default Footer;
