import React from "react";

const ListGroup = ({
  items,
  selectedItem,
  valueProperty: key,
  textProperty: value,
  onItemSelect
}) => {
  return (
    <ul className="list-group">
      {items.map(item => {
        return (
          <li
            className={
              selectedItem === item
                ? "list-group-item active"
                : "list-group-item"
            }
            key={item[key]}
            onClick={() => onItemSelect(item)}>
            {item[value]}
          </li>
        );
      })}
    </ul>
  );
};

// default values for properties
ListGroup.defaultProps = {
  textProperty: "name",
  valueProperty: "id"
};

export default ListGroup;
