import React, { Fragment } from "react";

const TableItemCount = ({ totalCount }) => {
  return (
    <Fragment>
      <p>Showing {totalCount} words in this course.</p>
    </Fragment>
  );
};

export default TableItemCount;
