import React, { Fragment } from "react";
import _ from "lodash";
import PropTypes from "prop-types";

const Pagination = ({ itemsCount, pageSize, onPageChange, currentPage }) => {
  const pagesCount = Math.ceil(itemsCount / pageSize);

  if (pagesCount === 1) return null; // dont display the pagination if there is only 1 page worth of data

  const pages = _.range(1, pagesCount + 1);

  return (
    <Fragment>
      <div className="row justify-content-center">
        <nav>
          <ul className="pagination">
            {/* <li className="page-item">
          <a className="page-link" href="#">
            Previous
          </a>
        </li> */}

            {pages.map(page => {
              return (
                <li
                  className={
                    currentPage === page ? "page-item active" : "page-item"
                  }
                  key={page}>
                  <button
                    className="page-link btn btn-link"
                    onClick={() => onPageChange(page)}>
                    {page}
                  </button>
                </li>
              );
            })}
            {/* <li className="page-item">
          <a className="page-link" href="#">
            Next
          </a>
        </li> */}
          </ul>
        </nav>
      </div>
    </Fragment>
  );
};

// Here we set rules for the properties on this pagination object
Pagination.propTypes = {
  itemsCount: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired
};

export default Pagination;
