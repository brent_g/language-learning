import React, { Component } from "react";
// import PropTypes from "prop-types";

// columns: []
// sortColumn : {}
// onSort: fn()

class TableHeader extends Component {
  raiseSort = path => {
    if (!path) throw new Error("Missing Path Argument");

    const sortColumn = { ...this.props.sortColumn };
    if (sortColumn.path === path) {
      sortColumn.order = sortColumn.order === "asc" ? "desc" : "asc";
    } else {
      sortColumn.path = path;
      sortColumn.order = "asc";
    }
    this.props.onSort(sortColumn);
  };

  renderSortIcon = column => {
    if (!column) throw new Error("Missing Column Argument");

    const sortColumn = { ...this.props.sortColumn };
    if (column.path !== sortColumn.path) return null;
    if (sortColumn.order === "asc") return <i className="fa fa-sort-asc" />;
    return <i className="fa fa-sort-desc" />;
  };

  render() {
    return (
      <thead>
        <tr>
          {this.props.columns.map(column => (
            <th
              className="clickable"
              scope="col"
              key={column.path || column.key}
              onClick={() =>
                column.path ? this.raiseSort(column.path) : null
              }>
              {column.label} {this.renderSortIcon(column)}
            </th>
          ))}
          <th />
        </tr>
      </thead>
    );
  }
}

// required properties
// TableHeader.propTypes = {
//   columns: PropTypes.array.isRequired,
//   sortColumn: PropTypes.object.isRequired,
//   onSort: PropTypes.func.isRequired
// };

export default TableHeader;
