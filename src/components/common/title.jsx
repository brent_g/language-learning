import React, { Fragment } from "react";
const Title = ({ text }) => {
  return (
    <Fragment>
      <h1 className="display-4 text-center">{text}</h1>
    </Fragment>
  );
};

export default Title;
