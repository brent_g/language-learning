import React, { Fragment } from "react";
import { Link } from "react-router-dom";

const Help = () => {
  return (
    <Fragment>
      <div className="row">
        <section className="border p-4 mt-2 mb-2">
          <h3>How to Use...</h3>
          <p>
            Go to <Link to="/courses">Courses</Link> from the navication menu
            and select a course from the list of courses on left hand side of
            the page.
          </p>
          <p>
            After selecting a course, Click on the{" "}
            <span className="btn btn-sm btn-primary">Continue</span> button to
            test your knowledge.
          </p>
          <p>
            Type the proper translation into the form and press the submit
            button (or press enter on your keyboard) and the results of your
            answer will be tallied in the Correct or Incorrect counters.
          </p>
          <h5>To see results</h5>
          <p>
            When you are finished practicing, you can press the{" "}
            <span className="btn btn-sm btn-primary">Results</span> button to
            see a breakdown of your correct and incorrect answers. You can use
            this to see which words you need to work on memorizing.
          </p>
          <p className="p-2">
            <b>OR</b>
          </p>
          <p>
            You can click on the{" "}
            <span className="btn btn-sm btn-secondary">Back</span> to return to
            the <Link to="/courses">Courses</Link> page and pick a different
            course.
          </p>
        </section>
      </div>
      <div className="row">
        <section className="border p-4 mt-2 mb-2">
          <h3>Add / Edit a Course</h3>
          <p>
            To add a new course or edit an existing one you need to be logged in
            using the <Link to="/login">Login</Link> form which can be accessed
            from the navigation menu. After you are logged in, you will see
            additional buttons available to you on the{" "}
            <Link to="/courses">Courses</Link> page.
          </p>
          <h5>Adding a new Course</h5>
          <p>
            To add a new course to the courses list you need to select the{" "}
            <span className="btn btn-sm btn-success">
              <i className="fa fa-plus" /> New Course button
            </span>{" "}
            underneath the courses list.
          </p>
          <h5>Editing a Course</h5>
          <p>
            Select a course from the courses list you want to edit then select
            the{" "}
            <span class="btn btn-sm btn-warning">
              <i class="fa fa-pencil" /> Edit
            </span>{" "}
            button above the course word table.
          </p>
          <p className="bg-warning text-dark align-center p-2 text-center">
            <i className="fa fa-exclamation-circle" /> Make sure to click save
            when making changes to a course or click cancel before saving to go
            back to the courses list.
          </p>
        </section>
      </div>
    </Fragment>
  );
};

export default Help;
