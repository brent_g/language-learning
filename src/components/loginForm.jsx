import React, { Fragment } from "react";
import Form from "./common/form";
import Joi from "joi-browser";

class LoginForm extends Form {
  state = {
    data: {
      email: "",
      password: ""
    },
    errors: {}
  };

  schema = {
    email: Joi.string()
      .email()
      .required()
      .label("Email"),
    password: Joi.string()
      .required()
      .label("Password")
  };

  doSubmit = () => {
    // make a call to the server
    console.log("Submitted!");
  };

  render() {
    return (
      <Fragment>
        <div className="row">
          <div className="col-6 m-auto">
            <h1 className="display-4">Login</h1>
            <form onSubmit={this.handleSubmit}>
              {this.renderInput("email", "Email", "email")}
              {this.renderInput("password", "Password", "password")}
              {this.renderSubmitButton("Login")}
            </form>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default LoginForm;
