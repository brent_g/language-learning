import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import Score from "./score";

// services
import { getCourse } from "../services/courseService";

class TestForm extends Component {
  state = {
    words: [],
    selection: null,
    inputValue: "",
    isCorrect: null,
    results: {
      correct: [],
      incorrect: []
    },
    hint: ""
  };

  componentWillMount() {
    this.populateCourse();
  }

  async populateCourse() {
    try {
      const courseId = this.props.match.params.id;
      const { data } = await getCourse(courseId);
      // use the maptoviewmodel to load the required fields into the state data object
      // and to get rid of unused mongo db fields (ex. __v)
      this.setState({
        selectedCourse: data,
        words: data.words,
        selection: this.getRandomWord(data.words)
      });
    } catch (ex) {
      // if the course with the given id doesnt exist, redirect to 404 page
      if (ex.response && ex.response.status === 404) {
        this.props.history.replace("/not-found");
      }
    }
  }

  handleChange = event => {
    this.setState({ inputValue: event.target.value });
  };

  handleKeyPress = event => {
    if (event.key === "Enter") {
      this.validateForm();
    }
  };

  handleClick = event => {
    event.preventDefault();
    this.validateForm();
  };

  validateForm = () => {
    if (this.inputValue !== "") {
      this.submitGuess();
      this.resetWordState();
      this.setRandomWord();
    }
    console.log("send a warning that there is no input entry");
  };

  isGuessCorrect = () => {
    const { inputValue, selection } = this.state;
    const words = [selection.origin, ...selection.alternate];
    return words.includes(inputValue);
  };

  // merge the alternatives with the origin and find in array
  submitGuess = () => {
    const { correct, incorrect } = this.state.results;
    const selection = this.state.selection;

    let _correct = correct;
    let _incorrect = incorrect;

    if (this.isGuessCorrect()) {
      // push the word into the correct results array so we can keep track of the results
      _correct.push(selection);
      this.setState({ correct: _correct });
      this.setState({ isCorrect: true });
    } else {
      // push the word into the incorrect results array so we can keep track of the results
      _incorrect.push(selection);
      this.setState({ incorrect: _incorrect });
      this.setState({ isCorrect: false });
    }
  };

  getRandomWord = words => {
    if (!words) throw new Error("Words array missing");

    const randomIndex = Math.floor(Math.random() * words.length);
    return words[randomIndex];
  };

  setRandomWord = () => {
    this.setState({ selection: this.getRandomWord(this.state.words) });
  };

  resetWordState = () => {
    this.setState({ selection: null });
    this.setState({ inputValue: "" });
  };

  displayCurrentWord = () => {
    return this.state.selection || "No Words Found";
  };

  showHint = () => {};

  render() {
    const { correct, incorrect } = this.state.results;
    return (
      <Fragment>
        <div className="row align-items-center h-50">
          <div className="col mx-auto">
            <div className="row justify-content-center mt-3">
              <h1 className="cover-heading">
                {this.displayCurrentWord().target}
              </h1>
            </div>
            {/* <div className="row justify-content-center mt-3">
          <h1 className="cover-heading" onClick={this.showAnswer}>
            {this.displayCurrentWord().origin}
          </h1>
        </div> */}
            <Score
              correctCount={correct.length}
              incorrectCount={incorrect.length}
            />
            <div className="row justify-content-center">
              <div className="col-6 input-group lead">
                <input
                  className="form-control form-control-lg"
                  type="text"
                  onChange={this.handleChange}
                  onKeyPress={this.handleKeyPress}
                  value={this.state.inputValue}
                  style={{ textAlign: "center" }}
                />
                <div className="input-group-append">
                  <button
                    className="input-group-button btn btn-lg btn-primary"
                    onClick={this.handleClick}>
                    Submit
                  </button>
                </div>
              </div>
            </div>
            <div className="row justify-content-center mt-3">
              <div className="col-0" />
            </div>
            <div className="row justify-content-center mt-3">
              <div className="col-1">
                <Link to="/courses">
                  <button className="btn btn-lg btn-secondary">Back</button>
                </Link>
              </div>
              <div className="col-1">
                <Link
                  to={{
                    pathname: `/results/`,
                    state: {
                      selectedCourse: this.state.selectedCourse,
                      results: this.state.results
                    }
                  }}>
                  <button className="btn btn-lg btn-primary">Results</button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default TestForm;
