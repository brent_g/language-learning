import React, { Component, Fragment } from "react";
import _ from "lodash";

import ResultsTable from "./resultsTable";

class Results extends Component {
  state = {};

  processResults = array => {
    const keys = _(array)
      .uniq()
      .value();

    const occurences = keys.map(key => {
      let count = _.countBy(array, item => {
        return key === item;
      });

      return {
        origin: key.origin,
        target: key.target,
        count: count.true
      };
    });

    return occurences;
  };

  render() {
    const { selectedCourse: course, results } = this.props.location.state;

    return (
      <Fragment>
        <h1>{course.label}</h1>
        <div className="row">
          <div className="col-5">
            <h3>Correct ({results.correct.length})</h3>
            <div className="row">
              <ResultsTable data={this.processResults(results.correct)} />
            </div>
          </div>
          <div className="col" />
          <div className="col-5">
            <h3>Incorrect ({results.incorrect.length})</h3>
            <div className="row">
              <ResultsTable data={this.processResults(results.incorrect)} />
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
export default Results;
