import React, { Fragment } from "react";

const Score = ({ correctCount: correct, incorrectCount: incorrect }) => {
  const total = correct + incorrect;

  const calculatePercent = score => {
    return Math.ceil((100 * score) / total || 0);
  };

  const correctPercent = calculatePercent(correct);
  const incorrectPercent = calculatePercent(incorrect);

  return (
    <Fragment>
      <div className="mb-4">
        <div className="row justify-content-center">
          <div className="col-2">
            <p className="text-center">Correct: {correct}</p>
          </div>
          <div className="col-2">
            <p className="text-center">Incorrect: {incorrect}</p>
          </div>
        </div>
        <div className="row justify-content-center">
          <div className="col-4">
            <div className="progress">
              <div
                className="progress-bar bg-success"
                role="progressbar"
                style={{ width: `${correctPercent}%` }}
              />
              <div
                className="progress-bar bg-danger"
                role="progressbar"
                style={{ width: `${incorrectPercent}%` }}
              />
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Score;
