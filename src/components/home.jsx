import React, { Fragment } from "react";

const Home = props => {
  return (
    <Fragment>
      <div className="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
        <header className="masthead" />

        <main role="main" className="inner cover">
          <h1 className="cover-heading display-4">Cover your page.</h1>
          <p className="lead">
            Cover is a one-page template for building simple and beautiful home
            pages. Download, edit the text, and add your own fullscreen
            background photo to make it your own.
          </p>
          <p className="lead">
            <button className="btn btn-lg btn-secondary">Learn more</button>
          </p>
        </main>

        <footer className="mastfoot mt-auto" />
      </div>
    </Fragment>
  );
};

export default Home;
