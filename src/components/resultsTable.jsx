import React, { Component, Fragment } from "react";
import Table from "./common/table";

class ResultsTable extends Component {
  state = {
    sortColumn: {
      path: "count",
      order: "desc"
    }
  };

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  columns = [
    { path: "origin", label: "Word" },
    { path: "target", label: "Translation" },
    { path: "count", label: "Count" }
  ];
  render() {
    const { data } = this.props;

    return (
      <Fragment>
        <Table
          columns={this.columns}
          sortColumn={this.state.sortColumn}
          data={data}
          onSort={this.handleSort}
        />
      </Fragment>
    );
  }
}

export default ResultsTable;
