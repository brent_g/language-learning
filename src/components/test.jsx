import React, { Fragment } from "react";
const Test = () => {
  return (
    <Fragment>
      <div className="container">
        <div
          className="row d-flex justify-content-between align-items-center"
          style={{ height: "300px" }}>
          <button className="btn btn-primary">Fun</button>
          <button className="btn btn-primary">Content</button>
          <button className="btn btn-primary">Goes</button>
          <button className="btn btn-primary">here</button>
        </div>
      </div>
    </Fragment>
  );
};

export default Test;
