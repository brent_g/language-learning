// 3rd party Components
import React, { Component, Fragment } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { ToastContainer } from "react-toastify";

// Components
import Home from "./components/home";
import Navigation from "./components/navigation";
import Courses from "./components/courses/courses";
import CourseForm from "./components/courses/courseForm";
import Results from "./components/results";
import TestForm from "./components/testForm";
import Admin from "./components/admin/dashboard";
import LoginForm from "./components/loginForm";
import About from "./components/about";
import Help from "./components/help";
import Test from "./components/test";
import NotFound from "./components/common/notFound";

// Services

// Style
import "./App.css";
import "react-toastify/dist/ReactToastify.css";

class App extends Component {
  render() {
    return (
      <Fragment>
        <ToastContainer />
        <Navigation />
        <main className="container h-100">
          <Switch>
            {/* Routes */}
            <Route path="/courses/new" component={CourseForm} />
            <Route path="/courses/edit/:id" component={CourseForm} />
            <Route path="/courses/edit/" component={NotFound} />
            <Route path="/courses/:id" component={TestForm} />
            <Route path="/courses" component={Courses} />
            <Route path="/results" component={Results} />
            <Route path="/admin" component={Admin} />
            <Route path="/home" component={Home} />
            <Route path="/login" component={LoginForm} />
            <Route path="/about" component={About} />
            <Route path="/help" component={Help} />
            <Route path="/test" component={Test} />
            <Route path="/not-found" component={NotFound} />

            {/* Redirects */}
            <Redirect from="/messages" to="results" />
            <Redirect from="/" to="home" exact />
            <Redirect to="/not-found" />
          </Switch>
        </main>
      </Fragment>
    );
  }
}

export default App;
