import http from "./httpService";

const apiEndPoint = "api/courses";

function courseUrl(courseId) {
  return `${apiEndPoint}/${courseId}`;
}

export function getCourses() {
  return http.get(apiEndPoint);
}

export function getCourse(id) {
  return http.get(courseUrl(id));
}

export function saveCourse(course) {
  // update
  if (course._id) {
    console.log("we are saving");
    /* we remove the id from the body object because the id is already part of the api endpoint url
      as a best practice,  we shouldnt have it available in 2 different places */
    const body = { ...course };
    delete body._id;
    return http.put(courseUrl(course._id), body);
  }
  // create
  return http.post(apiEndPoint, course);
}

export function deleteCourse(courseId) {
  return http.delete(courseUrl(courseId));
}
