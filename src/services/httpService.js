import axios from "axios";
import toast from "react-toastify";
import logger from "./logService";

axios.defaults.baseURL = "http://localhost:2000/";

// unexpected errors (network is down, server is down, db is down, bug in code)
// - the errors
// - display generic and friendly error message
axios.interceptors.response.use(null, error => {
  const expectedError =
    error.response &&
    error.response.status >= 400 &&
    error.response.status < 500;

  if (!expectedError) {
    logger.log(error);
    toast.error("An unexpected error occured.");
  }

  return Promise.reject(error);
});

export default {
  get: axios.get,
  post: axios.post,
  put: axios.put,
  delete: axios.delete
};
