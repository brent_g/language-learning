import * as Sentry from "@sentry/browser";

function init() {
  Sentry.init({
    dsn: "https://a80b82d29a7d47b2b32be55f587b0db2@sentry.io/1411817"
  });
}

function log(error) {
  Sentry.captureException(error);
}

export default {
  init,
  log
};
