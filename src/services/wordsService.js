const course = {
  id: 1,
  name: "Genki 1 Textbook: Page 40 - 41",
  words: [
    {
      origin: "um",
      target: "あの",
      alternate: ["ano"],
      type: [""],
      tags: []
    },
    {
      origin: "now",
      target: "いま",
      alternate: ["ima"],
      type: [""],
      tags: []
    },

    {
      origin: "English",
      target: "えいご",
      alternate: ["eego"],
      type: [""],
      tags: []
    },
    {
      origin: "yes",
      target: "ええ",
      alternate: ["ee"],
      type: ["casual"],
      tags: []
    },
    {
      origin: "student",
      target: "がくせえ",
      alternate: ["gakusee"],
      type: [""],
      tags: []
    },
    {
      origin: "language",
      target: "ご",
      alternate: ["go"],
      type: [""],
      tags: []
    },
    {
      origin: "high school",
      target: "こうこう",
      alternate: ["kookoo", "highschool"],
      type: [""],
      tags: []
    },
    {
      origin: "P.M",
      target: "ごご",
      alternate: ["gogo", "pm", "p.m"],
      type: [""],
      tags: []
    },
    {
      origin: "A.M",
      target: "ごぜん",
      alternate: ["gozen", "am", "a.m"],
      type: [""],
      tags: []
    },
    {
      origin: "years old",
      target: "さい",
      alternate: ["sai"],
      type: [""],
      tags: []
    },
    {
      origin: "Mr./Mrs.",
      target: "さん",
      alternate: ["mr", "mrs", "Mr", "Mrs", "Mr.", "Mrs."],
      type: [""],
      tags: []
    },
    {
      origin: "o'clock",
      target: "じ",
      alternate: ["ji"],
      type: [""],
      tags: []
    },
    {
      origin: "people",
      target: "じん",
      alternate: ["jin"],
      type: [""],
      tags: []
    },
    {
      origin: "Major",
      target: "せんこう",
      alternate: ["senkoo"],
      type: [""],
      tags: []
    },
    {
      origin: "Teacher",
      target: "せんせい",
      alternate: ["sensee"],
      type: [""],
      tags: []
    },
    {
      origin: "That's Right",
      target: "そうです",
      alternate: ["sou desu"],
      type: [""],
      tags: []
    },
    {
      origin: "Is that so?",
      target: "そうですか",
      alternate: ["sou desu ka"],
      type: [""],
      tags: []
    },
    {
      origin: "College; University",
      target: "だいがく",
      alternate: ["daigaku", "college", "university"],
      type: [""],
      tags: []
    },
    {
      origin: "Telephone",
      target: "でんわ",
      alternate: ["denwa", "telephone"],
      type: [""],
      tags: []
    },
    {
      origin: "Friend",
      target: "ともだち",
      alternate: ["tomodachi", "friend"],
      type: [""],
      tags: []
    },
    {
      origin: "Name",
      target: "なまえ",
      alternate: ["namae", "name"],
      type: [""],
      tags: []
    },
    {
      origin: "What",
      target: "なに",
      alternate: ["なん", "nan", "nani"],
      type: [""],
      tags: []
    },
    {
      origin: "Japan",
      target: "にほん",
      alternate: ["nihon"],
      type: [""],
      tags: []
    },
    {
      origin: "...year student",
      target: "ねんせえ",
      alternate: ["nensee"],
      type: [""],
      tags: []
    },
    {
      origin: "yes",
      target: "はい",
      alternate: ["hai"],
      type: ["formal"],
      tags: []
    },
    {
      origin: "half [past]",
      target: "はん",
      alternate: ["han"],
      type: ["time"],
      tags: []
    },
    {
      origin: "number",
      target: "ばんごう",
      alternate: ["bangoo"],
      type: ["phone"],
      tags: []
    },
    {
      origin: "International Student",
      target: "りゅうがくせい",
      alternate: ["ryuugakusee", "international student"],
      type: [""],
      tags: []
    },
    {
      origin: "I",
      target: "わたし",
      alternate: ["watashi"],
      type: [""],
      tags: []
    }
  ]
};

export function getWords() {
  return course;
}
