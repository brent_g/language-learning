import _ from "lodash";

export function paginate(items, pageNumber, pageSize) {
  const startIndex = (pageNumber - 1) * pageSize;
  // wrap the items array in a lodash function so we can chain lodash functions to make this more readable
  return _(items)
    .slice(startIndex)
    .take(pageSize)
    .value(); // .value() returns an array instead of a lodash wrapper
}
